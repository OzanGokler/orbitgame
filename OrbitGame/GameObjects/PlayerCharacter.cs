﻿
using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrbitGame
{
    class PlayerCharacter : AnimatedSprite
    {
        GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);

        public PlayerCharacter(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
            
        }


        public void  Update()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            { 
                if(Position.Y >10)
                {
                    Position.Y -= 1;
                    ChangeAnimation("walkup", false);
                }
            }
                //buraya denemelik yazıyorum 
            else if (gamePadState.Buttons.X == ButtonState.Pressed)
            {
                if (Position.Y < 430)
                {
                    Position.Y += 1;
                    ChangeAnimation("walkdown", false);
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                if (Position.Y < 430)
                {
                    Position.Y += 1;
                    ChangeAnimation("walkdown", false);
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                if (Position.X > 10)
                {
                    Position.X -= 1;
                    ChangeAnimation("walkleft", false);
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                if (Position.X < 430)
                {
                    Position.X += 1;
                    ChangeAnimation("walkright", false);
                }
            }
            else
            {
                ChangeAnimation("idle");
            }


        }
    }
}
