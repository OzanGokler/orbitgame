﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrbitGame.GameObjects
{
    class MonsterCharacter : AnimatedSprite
    {
        public MonsterCharacter(AnimatedSpriteSequences Meta) : base(Meta)
        {
            Position = new Vector2(100, 100);
        }
        Direction CurrentDirection = Direction.Right;

        enum Direction
        {
            Left,
            Right
        }

        internal void Update()
        {
            if (CurrentDirection == Direction.Right)
            {
                Position.X += 1;
                if(Position.X > 300)
                {
                    CurrentDirection = Direction.Left;
                }
            }
            if (CurrentDirection == Direction.Left)
            {
                Position.X -= 1;
                if (Position.X < 200)
                {
                    CurrentDirection = Direction.Right;
                }
            }
        }
    }
}
