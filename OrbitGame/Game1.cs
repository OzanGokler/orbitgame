﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OrbitGame.GameObjects;

namespace OrbitGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
     
        Texture2D imgBackground;
        Texture2D imgChar;
        Rectangle rBackground = new Rectangle(0, 0, 800, 600);
        PlayerCharacter TheCharacter;
        MonsterCharacter TheMonster;
        bool bGameOver = false;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            SpriteManager.ContentShell = Content;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            SpriteManager.GameTime = 0;
            // TODO: Add your initialization logic here
            //TheCharacter = new PlayerCharacter();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteManager.spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here
            imgBackground = Content.Load<Texture2D>("Sprites/background");
            TheCharacter = AnimationSpecReader.PopulateAnimations<PlayerCharacter>(@".\Content\Specs\Player1.json");
            TheMonster = AnimationSpecReader.PopulateAnimations<MonsterCharacter>(@".\Content\Specs\Monster.json");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            SpriteManager.GameTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if(bGameOver)
            {
                return;
            }
            TheCharacter.Update();
            TheMonster.Update();
            Rectangle PlayerBounds = TheCharacter.Bounds;
            Rectangle MonsterBounds = TheMonster.Bounds;
            if(PlayerBounds.Intersects(MonsterBounds))
            {
                TheCharacter.ChangeAnimation("die");
                bGameOver = true;
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
 
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            // TODO: Add your drawing code here
            SpriteManager.spriteBatch.Begin();
            SpriteManager.spriteBatch.Draw(imgBackground, rBackground, Color.White);
            TheCharacter.Draw();
            TheMonster.Draw();
            SpriteManager.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
